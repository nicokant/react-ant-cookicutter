const { override, fixBabelImports, addLessLoader } = require('customize-cra');

module.exports = override(
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true,
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: {
      /* These variables allows to change the ant theme */
      '@primary-color': '#00a69a',
      '@layout-sider-background': '#e6efee',
      '@layout-header-background': '#37474f',
      '@layout-header-color': '#fff',
      '@layout-body-background': '#f7f7f7',
      '@form-item-label-font-size': '17px',
    },
  }),
);
