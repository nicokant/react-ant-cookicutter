import React, { useEffect } from 'react';
import SpinnerPage from '../components/SpinnerPage';
import { useUser } from '../contexts/user';

const loadAuthenticatedApp = () => import('./AuthenticatedApp');
const AuthenticatedApp = React.lazy(loadAuthenticatedApp);
const UnauthenticatedApp = React.lazy(() => import('./UnauthenticatedApp'));

function App() {
  const user = useUser();

  useEffect(() => {
    loadAuthenticatedApp();
  }, []);

  console.log(user);

  return (
    <React.Suspense fallback={<SpinnerPage />}>
      {user ? <AuthenticatedApp /> : <UnauthenticatedApp />}
    </React.Suspense>
  );
}

export default App;
