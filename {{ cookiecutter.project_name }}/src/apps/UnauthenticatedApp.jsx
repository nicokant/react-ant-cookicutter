import React from 'react';
import { Route, Switch } from 'react-router-dom';
import SpinnerPage from '../components/SpinnerPage';

const LoginPage = React.lazy(() => import('../pages/Login'));

function UnauthenticatedApp() {
  return (
    <React.Suspense fallback={<SpinnerPage />}>
      <Switch>
        <Route path="" component={LoginPage} />
      </Switch>
    </React.Suspense>
  );
}

export default UnauthenticatedApp;
