import React from 'react';
import AppLayout from "../components/appLayout"
import { Switch, Route, Redirect } from "react-router-dom"

function AuthenticatedApp() {
  return (
    <AppLayout>
      <Switch>
        <Route exact path="/home/" render={() => <h2>Home</h2>} />
        <Route exact path="/" render={() => <Redirect to="/home/" />} />
      </Switch>
    </AppLayout>
  );
}

export default AuthenticatedApp;
