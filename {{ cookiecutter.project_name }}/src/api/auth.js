import client from './client';
import { LOCALSTORAGE_TOKEN_KEY } from '../constants';

export async function login({ email, password }) {
  const res = await client('auth/login/', { body: { email, password } });
/*  if (!res.error) {
    return res.data.ephemeral_token;
  }
  throw res.data.non_field_errors.join(' ');
}

export async function mfaLogin(data) {
  const res = await client('auth/login/code/', { body: data }); */
  if (!res.error) {
    window.localStorage.setItem(LOCALSTORAGE_TOKEN_KEY, res.data.auth_token);
    return res.data.auth_token;
  }
  throw res.data.non_field_errors.join(' ');
}

export async function logout() {
  await client('auth/logout/', { method: 'POST' });
  window.localStorage.removeItem(LOCALSTORAGE_TOKEN_KEY);
}

/* export async function register(data) {
  return client('auth/users/', { body: data, multipart: true });
} */
