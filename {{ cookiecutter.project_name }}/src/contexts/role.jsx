import React from 'react';
import { useAuth } from './auth';

const RoleContext = React.createContext();

function RoleProvider(props) {
  const {
    data: { user },
  } = useAuth();
  const role = user && user.role ? user.role : null;
  return <RoleContext.Provider value={role} {...props} />;
}

function useRole() {
  const context = React.useContext(RoleContext);
  if (context === undefined) {
    throw new Error(`useRole must be used within a RoleProvider`);
  }
  return context;
}

export { RoleProvider, useRole };
