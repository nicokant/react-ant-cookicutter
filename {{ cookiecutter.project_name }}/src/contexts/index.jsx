import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { AuthProvider } from './auth';
import { UserProvider } from "./user"
import { RoleProvider } from "./role"

function AppProviders({ children }) {
  return (
    <BrowserRouter>
      <AuthProvider>
        <UserProvider>
          <RoleProvider>
            {children}
          </RoleProvider>
        </UserProvider>
      </AuthProvider>
    </BrowserRouter>
  );
}

export default AppProviders;
