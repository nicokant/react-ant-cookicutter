import React from 'react';
import { Button, Form, Icon, Input } from 'antd';

function LoginForm({ login }) {
  return (
    <Form onFinish={login} className="login-form">
      <Form.Item name="email" rules={[{ required: true, message: 'Inserisci il tuo indirizzo email' }]} >
        <Input prefix={<Icon type="user" style={{ fontSize: 13 }} />} placeholder="Email" size="large" />
      </Form.Item>

      <Form.Item name="password" rules={[{ required: true, message: 'Inserisci la password' }]}>
        <Input
          prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
          type="password"
          placeholder="Password"
          size="large"
        />
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button" size="large">
          Accedi
        </Button>
      </Form.Item>
    </Form>
  );
}

export default LoginForm;

LoginForm.propTypes = {};
