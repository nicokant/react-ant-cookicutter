import React, { useCallback } from 'react';
import { Layout, notification, Typography } from "antd"
import { useHistory } from 'react-router-dom';
import LoginForm from './LoginForm';
import * as authClient from '../../api/auth';
import { useAuth } from '../../contexts/auth';

function LoginPage() {
  const history = useHistory();
  const { reload } = useAuth();

  const emailPasswordLogin = useCallback(
    async data => {
      try {
        const response = await authClient.login(data);
        console.log(response);
        reload();
        history.replace('/');
      } catch (e) {
        console.log(e);
        notification.error({ message: e });
      }
    },
    [],
  );

  return (
    <Layout id="app-container" className="login" style={{ height: '100vh' }}>
        <Layout.Content style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', padding: '3rem 0' }}>
          <div style={{ maxWidth: '50em', margin: 20 }}>
            <Typography.Title>Login</Typography.Title>
            <LoginForm login={emailPasswordLogin} />
          </div>
        </Layout.Content>
    </Layout>
  );
}

export default LoginPage;
