import React from 'react';
import { Layout, Spin } from 'antd';

function SpinnerPage() {
  return (
    <Layout style={{ height: '100vh' }}>
      <Layout.Content
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Spin size="large" />
      </Layout.Content>
    </Layout>
  );
}

export default SpinnerPage;
