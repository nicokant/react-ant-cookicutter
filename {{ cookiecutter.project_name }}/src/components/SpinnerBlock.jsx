import React from 'react';
import { Layout, Spin } from 'antd';

function SpinnerBlock() {
  return (
    <Layout style={{ height: '30vh', background: 'transparent' }}>
      <Layout.Content
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Spin size="large" />
      </Layout.Content>
    </Layout>
  );
}

export default SpinnerBlock;
