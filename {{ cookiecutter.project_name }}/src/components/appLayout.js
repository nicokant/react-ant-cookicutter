import { Layout, Menu } from "antd"
import React from "react"
import {
  HomeOutlined,
} from "@ant-design/icons"
import { Link, useLocation } from "react-router-dom"

export default function AppLayout({ children }) {

  const location = useLocation();


  return (
    <Layout style={{ minHeight: '100vh'}}>
      <Layout.Sider>
        <Menu theme="dark" selectedKeys={location.pathname}>
          <Menu.Item key="/home/">
            <HomeOutlined />
            <span>Home</span>
            <Link to="/home/" />
          </Menu.Item>
        </Menu>
      </Layout.Sider>
      <Layout.Content>{children}</Layout.Content>
    </Layout>
  )
}
