import React from 'react';
import { ConfigProvider } from 'antd';
import ReactDOM from 'react-dom';

import 'moment/locale/it';
import it from 'antd/es/locale/it_IT';

import './errorReporting';
import './styles/index.scss';

import App from './App';
import AppProviders from './contexts';

ReactDOM.render(
  <ConfigProvider locale={it}>
    <AppProviders>
      <App />
    </AppProviders>
  </ConfigProvider>,
  document.getElementById('root'),
);
