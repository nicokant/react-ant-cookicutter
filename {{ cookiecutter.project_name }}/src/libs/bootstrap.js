import { getMe } from '../api/user';
import { LOCALSTORAGE_TOKEN_KEY } from '../constants';

async function bootstrap() {
  const token = localStorage.getItem(LOCALSTORAGE_TOKEN_KEY);
  if (!token) {
    return { user: null };
  }
  try {
    const user = await getMe();

    return { user };
  } catch (e) {
    return { user: null };
  }
}

export default bootstrap;
